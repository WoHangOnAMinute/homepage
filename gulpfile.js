/*!
 * gulp
 * $ npm install gulp-sass gulp-autoprefixer gulp-cssnano gulp-jshint gulp-concat gulp-uglify gulp-notify gulp-rename gulp-livereload gulp-cache del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    server = require('gulp-express'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del');

// HTML
gulp.task('html', function() {
  return gulp.src('src/*.html')
    .pipe(gulp.dest('dist'))
    .pipe(notify({ message: 'HTML task complete' }));
});

// Styles
gulp.task('styles', function() {
  return gulp.src('src/styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('dist/styles'))
    // .pipe(rename({ suffix: '.min' }))
    .pipe(notify({ message: 'Styles task complete' }));
  // return sass('src/styles/*.scss', { style: 'expanded' })
  //   // .pipe(autoprefixer('last 2 version'))
  //   .pipe(gulp.dest('dist'))
  //   // .pipe(cssnano())
  //   // .pipe(gulp.dest('dist/styles'))
});

gulp.task('style:images', () => {
  return gulp.src('src/styles/images/*').
    pipe(gulp.dest('dist'));
});

gulp.task('style:fonts', () => {
  return gulp.src('src/styles/fonts/*').
    pipe(gulp.dest('dist'));
});


gulp.task('copy:images', () => {
  return gulp.src('src/assets/images/*').
    pipe(gulp.dest('dist/assets/images'));
});

gulp.task('copy:cssImages', () => {
  return gulp.src('src/styles/images/*').
    pipe(gulp.dest('dist'));
});

// Scripts
gulp.task('scripts', function() {
  return gulp.src('src/scripts/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('dist'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Clean
gulp.task('clean', function() {
  return del(['dist/styles', 'dist/scripts']);
});


gulp.task('express', function () {
  // Start the server at the beginning of the task
  server.run(['./server.js']);
});

// Default task
gulp.task('build', ['clean'], function() {
  gulp.start('styles', 'style:images', 'style:fonts', 'scripts', 'html', 'copy:images', 'copy:cssImages');
});
// Default task
gulp.task('default', ['clean'], function() {
  gulp.start('styles', 'style:images', 'style:fonts', 'scripts', 'html', 'copy:images', 'copy:cssImages', 'express');
});

// Watch
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('src/styles/*.scss', ['styles']);

  // Watch .js files
  gulp.watch('src/scripts/**/*.js', ['scripts']);

  // Watch .html files
  gulp.watch('src/index.hmtl', ['html']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in dist/, reload on change
  gulp.watch(['dist/**']).on('change', livereload.changed);

});
